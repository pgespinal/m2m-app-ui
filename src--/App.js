import React, { Component } from "react";
import { Route } from "react-router-dom";
import Dashboard from "./Dashboard";
//import Profile from "./Profile";
//import Nav from "./Nav";
import AppBar from "./AppBar";
import Login from "./Login";

class App extends Component {
  render() {
    return (
      <>
        <AppBar/>
        <Route path="/dashboard" exact component={Dashboard} />
        <Route path="/" component={Login} />
      </>
    );
  }
}

export default App;
