import React, { Component } from "react";
import { Link } from "react-router-dom";

class Nav extends Component {
  render() {
    return (
      <nav>
        <ul>
          <li>
            <Link to="/">M2M</Link>
          </li>
          <li hidden="True">
            <Link to="profile">Profile</Link>
          </li>
        </ul>
        
      </nav>
    );
  }
}

export default Nav;
